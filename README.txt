D�ndu-se un schelet al aplica?iei, implementa?i urm�toarele clase:

seriaf.poo.tema1.CharCounterImpl - clasa trebuie s� implementeze interfa?a CharCounter ?i metoda definit� �n aceast� interfa?� (vezi comentariul din interfa?�)
seriaf.poo.tema1.CustomFileReaderImpl - clasa trebuie s� implementeze interfa?a CustomFileReader ?i metoda definit� �n aceast� interfa?� (vezi comentariul din interfa?�)
seriaf.poo.tema1.OccurenceComparator - clasa trebuie s� implementeze interfa?a Comparator pentru obiecte de tip CharOccurence ?i s� poat� fi folosit� pentru a ordona descresc�tor caracterele �n func?ie de num�rul de apari?ii.