package seriaf.poo.tema1;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

/**
 *
 * @author Ana
 */
public class CustomFileReaderImpl implements CustomFileReader {

    private String line = "";
    private String str = "";

    public String readFileContents(String fileName) throws IOException {

        FileReader fIn = new FileReader(fileName);
        BufferedReader in = new BufferedReader(fIn);
        while ((line = in.readLine()) != null) {
            str += line;
        }
        fIn.close();
        in.close();
        str = str.replaceAll("\\n", "");
        return str;
    }
}
