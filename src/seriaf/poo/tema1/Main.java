package seriaf.poo.tema1;

import java.io.IOException;
import java.util.Collections;
import java.util.List;

/**
 *
 * @author Ana
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        if (args.length == 0) {
            /* Pentru a simula in Netbeans un argument din linia de comanda, acest argument
             * se adauga in Arguments din fereastra care se deschide cand dati click pe
             * Run->Set Project Configuration->Customize
             */
            System.out.println("Numele fisierului trebuie dat ca argument programului.");
            return;
        }

        CustomFileReader customFileReader = new CustomFileReaderImpl();
        CharCounter counter = new CharCounterImpl();
        try {
            List<CharOccurence> list = counter.countChars(customFileReader.readFileContents(args[0]));

            Collections.sort(list, new OccurenceComparator());

            for (CharOccurence charOccurence : list) {
                System.out.println(charOccurence);
            }
        } catch (IOException ex) {
            System.err.println("Exceptie in citirea fisierului: " + ex.getMessage());
        }
    }
}
