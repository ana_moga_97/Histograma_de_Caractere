/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package seriaf.poo.tema1;

import java.util.Comparator;

/**
 *
 * @author Ana
 */
public class OccurenceComparator implements Comparator<CharOccurence> {

    public int compare(CharOccurence value1, CharOccurence value2) {
        return value2.getOccurences() - value1.getOccurences();
    }
}
