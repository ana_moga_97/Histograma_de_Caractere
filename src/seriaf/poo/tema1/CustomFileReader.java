package seriaf.poo.tema1;

import java.io.IOException;

/**
 *
 * @author Ana
 */
public interface CustomFileReader {

    /**
     * Metoda deschide fisierul specificat prin numele "filename", citeste tot
     * continutul lui intr-un String si intoarce acest String. String-ul nu
     * trebuie sa contina caracterele newline (\n).
     *
     * @param filename Numele fisierului care trebuie citit.
     * @return continutul fisierului ca un obiect de tip String, excluzand
     * caracterele newline (\n).
     * @throws IOException daca fisierul nu poate fi citit din orice motiv.
     */
    String readFileContents(String filename) throws IOException;
}
