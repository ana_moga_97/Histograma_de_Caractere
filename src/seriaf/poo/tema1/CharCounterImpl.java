/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package seriaf.poo.tema1;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Ana
 */
public class CharCounterImpl implements CharCounter {

    @Override
    public List<CharOccurence> countChars(String text) {
        List<CharOccurence> ourlist = (List<CharOccurence>) new ArrayList<CharOccurence>();
        HashMap<Character, Integer> charmap = (HashMap<Character, Integer>) new HashMap<Character, Integer>();
        char[] toProcess = text.toCharArray();
        for (char item : toProcess) {
            if (charmap.containsKey(item)) {
                charmap.put(item, charmap.get(item) + 1);
            } else {
                charmap.put(item, 1);
            }
        }
        for (Map.Entry obj : charmap.entrySet()) {
            CharOccurence e = new CharOccurence((char) obj.getKey(), (int) obj.getValue());
            ourlist.add(e);
        }
        return ourlist;
    }
}
