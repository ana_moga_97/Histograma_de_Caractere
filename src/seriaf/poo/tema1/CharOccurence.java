package seriaf.poo.tema1;

/**
 *
 * @author Ana
 */
public class CharOccurence {

    private final char mChar;
    private final int mOccurences;

    public CharOccurence(char newChar, int occurences) {
        mChar = newChar;
        mOccurences = occurences;
    }

    public int getOccurences() {
        return mOccurences;
    }

    public char getChar() {
        return mChar;
    }

    public String toString() {
        return mChar + ": " + mOccurences;
    }

    @Override
    public boolean equals(Object that) {
        return that instanceof CharOccurence
                && ((CharOccurence) that).mChar == mChar
                && ((CharOccurence) that).mOccurences == mOccurences;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 41 * hash + this.mChar;
        hash = 41 * hash + this.mOccurences;
        return hash;
    }
}
