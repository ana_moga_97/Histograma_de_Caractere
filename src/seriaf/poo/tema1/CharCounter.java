package seriaf.poo.tema1;

import java.util.List;

/**
 *
 * @author Ana
 */
public interface CharCounter {

    /**
     * Metoda numara caracterele din sirul "text" si intoarce o lista de obiecte
     * de tip CharOccurence, cate un obiect pentru fiecare caracter distinct din
     * text.
     *
     * @param text Textul ce va fi analizat.
     * @return o lista de obiecte de tip CharOccurence, cate un obiect pentru
     * fiecare caracter distinct din text.
     */
    List<CharOccurence> countChars(String text);
}
